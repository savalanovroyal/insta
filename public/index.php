<?php

require_once '../vendor/autoload.php';

if ( isset($_GET['username']) && !empty($_GET['username']) )
{
    $username = $_GET['username'];
    $maxId = ( isset($_GET['maxId']) )? $_GET['maxId'] : '';

    $instagram = new \InstagramScraper\Instagram();
    $response = $instagram->getPaginateMedias($username, $maxId);

    $data = $response;

    $data['medias'] = [];

    foreach ($response['medias'] as $row)
    {
        if ( $row->getType() == 'video' )
        {
            $data['medias'][] = [
                'id' => $row->getId(),
                'shortCode' => $row->getShortCode(),
                'createdTime' => $row->getCreatedTime(),
                'link' => $row->getLink(),
                'imageThumbnailUrl' => $row->getImageThumbnailUrl(),
            ];
        }
    }

    exit(json_encode($data));
}